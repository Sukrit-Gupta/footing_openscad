//This file models a footing without any reinforcement details

//depth of column, say:
d_col = 2000;

//assuming the column is at the center of the footing in both x and y axis.
//size of the column
//length along x-axis:
l_col = 400;
//length along y-axis:
b_col = 300;
//depth of footing, say:


//footing is assumed to be centered and base at height z
//length of footing:
l = 2500;
//width of footing:
b = 2000;
//Total Depth of footing:
d_f = 500;
//depth of footing at edges:
d_ed = 200;

// z-position of base of footing:
z = 0;

//calculations:
//upper points of footing:
p1 = [l/2,b/2,z+d_ed];
p2 = [l/2,-b/2,z+d_ed];
p3 = [-l/2,-b/2,z+d_ed];
p4 = [-l/2,b/2,z+d_ed];
//bottom points of column:
p5 = [l_col/2,b_col/2,d_f];
p6 = [l_col/2,-b_col/2,d_f];
p7 = [-l_col/2,-b_col/2,d_f];
p8 = [-l_col/2,b_col/2,d_f];

polyhedron(
points = [ p1, p2, p3, p4, p5,  p6, p7, p8], 
faces = [ [4,5,6,7], [0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7], [0,3,2,1] ]);

//column
translate([-l_col/2,-b_col/2,d_f])
cube([l_col,b_col,d_col]);

//bottom of footing
translate([-l/2,-b/2,z])
cube([l,b,d_ed]);